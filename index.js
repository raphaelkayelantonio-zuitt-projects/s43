// console.log("hello");
let posts = [];

let count = 1;

document.querySelector("#form-add-post").addEventListener("submit", (e) =>{


	e.preventDefault();

	posts.push({
		id: count, 
		title: document.querySelector("#txt-title").value,
		body:document.querySelector("#txt-body").value
	});

	count++;
	console.log(posts);
	alert("Successfully added!");
	showPosts();
});


const showPosts = () =>{
	let postEntries = "";

	posts.forEach((post) =>{
		postEntries +=`
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	})
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector (`#post-body-${id}`).innerHTML;
document.querySelector("#txt-edit-id").value = id;
document.querySelector("#txt-edit-title").value = title;
document.querySelector("#txt-edit-body").value = body;
}


document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();

	let postId = document.querySelector("#txt-edit-id").value
	let title = document.querySelector("#txt-edit-title").value
	let body = document.querySelector("#txt-edit-body").value

	for(i = 0; i< posts.length; i++){
		if(posts[i].id == postId){
			posts[i].title = title
			posts[i].body = body

			showPosts();

			alert("Successfully updated");
			break;
		}
	}	
})


const deletePost = (id) => {
	console.log(id)
	document.querySelector(`#post-${id}`).style.display = "none";
}

